﻿<%@ Page Title="Home Page" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Default2.aspx.cs" Inherits="Lab1.Default2" %>

<asp:Content runat="server" ID="FeaturedContent" ContentPlaceHolderID="FeaturedContent">
    <asp:Table runat="server" align="center">
        <asp:TableRow runat="server">
            <asp:TableCell>
                <asp:Panel runat="server" ID="pnlPanela1" Font-Names="Verdana" BackColor="WhiteSmoke">
                    <asp:TextBox runat="server" ID="txtOperand1" EnableViewState="false"></asp:TextBox>
                    <asp:TextBox runat="server" ID="txtOperand2" AutoPostBack="true" OnTextChanged="txtOperand2_TextChanged"></asp:TextBox>
                    <asp:Label runat="server" ID="lblRezultat" EnableViewState="false"></asp:Label>
                    <asp:Button runat="server" ID="btnSoberi" Text="Собери" OnClick="btnSoberi_Click"/>
                </asp:Panel>
            </asp:TableCell>
        </asp:TableRow>
    </asp:Table>
    <asp:CheckBox runat="server" ID="chbVidlivo"  Text="Видливо" OnCheckedChanged="chbVidlivo_CheckedChanged" AutoPostBack="true" Checked="true"/>
</asp:Content>
