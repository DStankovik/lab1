﻿<%@ Page Language="C#" AutoEventWireup="true" MasterPageFile="~/Site.Master" CodeBehind="Default3.aspx.cs" Inherits="Lab1.Default3" %>

<asp:Content runat="server" ContentPlaceHolderID="FeaturedContent">
    <asp:ValidationSummary runat="server" />
    <asp:TextBox runat="server" ID="txtLozinka" TextMode="Password" ></asp:TextBox>
    <asp:TextBox runat="server" ID="txtPoraka" TextMode="Multiline" Rows="5" ReadOnly="true" OnTextChanged="txtPoraka_TextChanged" AutoPostBack="true"></asp:TextBox>
    <br /><asp:Button runat="server" ID="btnProveri" OnClick="btnProveri_Click" Text="Провери" />
    <asp:Button runat="server" ID="btnPrvaStrana" Enabled="false" Text="Прва страна" OnClick="btnPrvaStrana_Click" />
</asp:Content>