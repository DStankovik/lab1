﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace Lab1
{
    public partial class Default3 : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
        }

        protected void btnProveri_Click(object sender, EventArgs e)
        {
            if (txtLozinka.Text != "мрежно програмирање")
            {
                ModelState.AddModelError("txtLozinka", "Incorect password");
            }
            else
            {
                txtPoraka.ReadOnly = false;
                txtPoraka.Focus();
            }
        }

        protected void txtPoraka_TextChanged(object sender, EventArgs e)
        {
            btnPrvaStrana.Enabled = true;
        }

        protected void btnPrvaStrana_Click(object sender, EventArgs e)
        {
            Response.Redirect("~/Default.aspx");
        }
    }
}